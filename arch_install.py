#! /usr/bin/python

import os
import subprocess as sp
import argparse
import configparser

#parse argument
parser = argparse.ArgumentParser()
parser.add_argument('device', type=str,
    help='the path to the device where the install will happen')
parser.add_argument('--encrypt', action='store_true',
    help='encrypt the entire installation besides the efi')
args = parser.parse_args()

#define important vars
container_size = '80G'
pacstrap_pkgs = ['base', 'base-devel', 'btrfs-progs', 'linux',
    'linux-firmware', 'neovim', 'sudo', 'grub', 'python',
    'efibootmgr']

#build paritition scheme
scheme = '2048,512M\n,' + container_size

#write changes to disk < echo 'scheme' | sfdisk dev ... >
layout = sp.Popen(['echo', scheme],
    stdout=sp.PIPE)
wipe_table = sp.run(['sfdisk', f'{args.device}'],
    stdin=layout.stdout)
layout.stdout.close() #good practice

efi_path = args.device + '1'
container_path = args.device + '2' #second partition holds os
install_path = container_path

#create config to pass to chroot because I see no easier way
config = configparser.ConfigParser()
config['DEFAULT'] = {'device_path': container_path, 'encrypted':'no'}

if args.encrypt:
    #inform chroot that disk is encrypted
    config['DEFAULT']['encrypted'] = 'yes'
    #format luks container
    print('FORMATING LUKS')
    sp.run(['cryptsetup', 'luksFormat', '--align-payload=8192',
        '-s', '256', '-c', 'aes-xts-plain64', '--type', 'luks1',
        container_path])

    #open container
    print('\nOPENNING LUKS FOR INSTALL')
    sp.run(['cryptsetup', 'open', container_path, 'system'])

    #change the install path to the new volume
    install_path = '/dev/mapper/system'

    #add cryptsetup to the pacstrap installation
    pacstrap_pkgs += ['cryptsetup']

#format partitions
print('\nMAKING FILESYSTEMS')
sp.run(['mkfs.fat', '-F32', efi_path])
sp.run(['mkfs.btrfs', '--force', '--label', 'SYSTEM', install_path])

#define some functions to make running commands easier
def make_subvolume(path):
    sp.run(['btrfs', 'subvolume', 'create', path])

def mount_subvolume(base_path, vol):
    if vol != 'default': #default is mounted on base_path
        base_path = os.path.join(base_path, vol)
    sp.run(['mount', '-t', 'btrfs', '-o',
        f'compress=lzo,subvol={vol},noatime', 'LABEL=SYSTEM',
        base_path])

#mount disk for building btrfs tree
subvols = ['default'] #root
subvols += ['snapshots', 'home'] #add new volumes here
subvols += ['swapfile']
system_path = '/mnt/system'
os.makedirs(system_path, exist_ok=True)
sp.run(['mount', '-t', 'btrfs', 'LABEL=SYSTEM', system_path])
for subvol in subvols:
    make_subvolume(os.path.join(system_path, subvol))

#remount to build filesystem hierarchy
sp.run(['umount', system_path])
mount_subvolume(system_path, subvols[0]) #assume first is root
os.makedirs(os.path.join(system_path, 'efi'))
sp.run(['mount', efi_path, os.path.join(system_path, 'efi')])
for name in subvols[1:]:
    os.makedirs(os.path.join(system_path, name))
    mount_subvolume(system_path, name)

#make subvolume for package cache
cache_path = os.path.join(system_path, 'var', 'cache', 'pacman')
os.makedirs(cache_path)
make_subvolume(os.path.join(cache_path, 'pkg'))

#build swapfile, subvol mount options dont really apply here
swap_file_path = os.path.join(system_path, subvols[-1], 'swp')
sp.run(['truncate', '-s', '0', swap_file_path])
sp.run(['chattr', '+C', swap_file_path])
sp.run(['btrfs', 'property', 'set', swap_file_path, 'compression',
    '""'])
sp.run(['dd', 'if=/dev/zero', f'of={swap_file_path}', 'bs=1M',
    'count=4096', 'status=progress']) #swap is 4G
sp.run(['chmod', '600', swap_file_path])
sp.run(['mkswap', swap_file_path])
sp.run(['swapon', swap_file_path])

#run pacstrap
pacstrap_command = ['pacstrap', system_path] + pacstrap_pkgs
sp.run(pacstrap_command)

#generate fstab
fstab = sp.run(['genfstab', '-U', system_path,],
    capture_output=True)
with open(os.path.join(system_path, 'etc', 'fstab'), 'w') as f:
    f.write(fstab.stdout.decode('utf-8'))

#set up chroot jail very hack but safe assumtions are made (efi)
chroot_script = 'arch_chroot.py'
sp.run(['mount', '-t', 'proc', 'none',
    os.path.join(system_path, 'proc')])
for elem in ['dev', 'sys', 'sys/firmware/efi/efivars']:
    sp.run(['mount', '-o', 'bind', '/'+elem,
    os.path.join(system_path, elem)])
sp.run(['mount', '--bind', '/etc/resolv.conf', #internet
    os.path.join(system_path, 'etc', 'resolv.conf')])
sp.run(['cp', chroot_script, system_path])

#write config to disk
config_path = os.path.join(system_path, 'install_config.ini')
with open(config_path, 'w') as f:
    config.write(f)

#chroot then clean up afterwards
sp.run(['chroot', system_path, './'+chroot_script])
sp.run(['rm', os.path.join(system_path, chroot_script)])
sp.run(['rm', config_path])
