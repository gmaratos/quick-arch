#!/usr/bin/python

import os
import configparser
import subprocess as sp

#set timezone
sp.run(['ln', '-sf', '/usr/share/zoneinfo/America/Chicago',
    '/etc/localtime'])

#set locale
with open('/etc/locale.gen', 'a') as f:
    f.write('en_GB.UTF-8 UTF-8\n\n')
sp.run(['locale-gen'])
with open('/etc/locale.conf', 'w') as f:
    f.write('LANG=en_GB.UTF-8')

#set hostname
hostname = 'eddison'
with open('/etc/hostname', 'w') as f:
    f.write(hostname)
with open('/etc/hosts', 'a') as f:
    f.write(f'127.0.1.1\t{hostname}.localdomain {hostname}')

#read configuration
config = configparser.ConfigParser()
config.read('/install_config.ini')

#install network manager
sp.run(['pacman', '-Sy', '--noconfirm', 'networkmanager'])

if config['DEFAULT']['encrypted'] == 'yes':
    device_path = config['DEFAULT']['device_path']
    #configure mkinitcpio.conf
    with open('/etc/mkinitcpio.conf', 'r') as f:
        mkinitcpio = f.read()
    mkinitcpio = mkinitcpio.replace('BINARIES=()',
        'BINARIES=(/usr/bin/btrfs)')
    mkinitcpio = mkinitcpio.replace('FILES=()',
        'FILES=(/root/crypt.keyfile)')
    mkinitcpio = mkinitcpio.replace(
        'HOOKS=(base udev autodetect modconf block filesystems'\
        ' keyboard fsck)', 'HOOKS=(base udev autodetect'\
        ' modconf block filesystems keyboard fsck encrypt)')
    with open('/etc/mkinitcpio.conf', 'w') as f:
        f.write(mkinitcpio)

    #generate keyfile
    sp.run(['dd', 'bs=512', 'count=4', 'if=/dev/random',
        'of=/root/crypt.keyfile', 'iflag=fullblock'])
    sp.run(['chmod', '000', '/root/crypt.keyfile'])
    sp.run(['cryptsetup', '-v', 'luksAddKey', device_path,
        '/root/crypt.keyfile'])

    #modify grub
    with open('/etc/default/grub', 'r') as f:
        grub = f.read()
    grub = grub.replace('GRUB_CMDLINE_LINUX=""',
        f'GRUB_CMDLINE_LINUX="cryptdevice={device_path}:luks:'\
        'allow-discards cryptkey=rootfs:/root/crypt.keyfile"')
    grub = grub.replace('#GRUB_ENABLE_CRYPTODISK',
        'GRUB_ENABLE_CRYPTODISK')
    with open('/etc/default/grub', 'w') as f:
        f.write(grub)

#enable the necessary packages
sp.run(['systemctl', 'enable', 'NetworkManager'])

#rebuild initramfs
sp.run(['mkinitcpio', '-P'])

#install grub
sp.run(['grub-install', '--target=x86_64-efi',
    '--efi-directory=/efi', '--bootloader-id=ARCH'])
sp.run(['grub-mkconfig', '-o', '/boot/grub/grub.cfg'])

print('SETTING ROOT PASSWORD')
sp.run(['passwd'])
