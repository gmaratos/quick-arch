# Install Arch Linux Quickly
The script *arch_install.py* can install arch linux from the install media. Its main purpose is to quickly configure the disk for full disk encryption, automate most of the initial steps of installing arch. It will wipe the data on the device you specify so make sure you are careful. There is no dual boot support. I have only tested them on my own machines. **Use at your own risk**.

## Initial Setup
1. Boot the archiso on the machine you want to install arch linux.
2. Install the prerequisite packages.
```
pacman -Syy glibc git btrfs-progs cryptsetup
```
3. Clone this repository

## Installing
Run *arch_install.py* in this repository, the only required argument is the path to the device you want to install on. You can optionally encrypt the entire disk by passing an argument at the command line. You can see whatever I currently call it by passing the *-h* flag without any arguments.

## Summary of Resulting System
After the script is done, the system will be very similar to what you would expect after following the wiki. A few things to note are:
1. Everything is in one partition, the file system is BTRFS.
2. The swap is a file in its own subvolume, mounted at */swapfile*.
3. Network Manager is enabled.
4. Other things installed: base-devel, linux-firmware, sudo, grub, python.
5. The kernel is not lts.

## Known Bugs
1. The locales might not be configured correctly after the installation has finished.
2. If your device is nvme, then the script will fail to mount the filesystem because of how it infers the name of created partitions.
